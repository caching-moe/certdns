package main

import (
	"context"
	"flag"
	"fmt"
	"gitlab.com/cachingmoe/certdns/cmd/server/certmanager"
	"gitlab.com/cachingmoe/certdns/cmd/server/config"
	"gitlab.com/cachingmoe/certdns/cmd/server/dns"
	"gitlab.com/cachingmoe/certdns/cmd/server/invoker"
	"gitlab.com/cachingmoe/certdns/cmd/server/server"
	"log"

	"go.uber.org/zap"
)

var (
	configPath = flag.String("config", "config.toml", "file path of config file")
)

func main() {
	err := run(context.Background())
	if err != nil {
		log.Printf("Fatal error: %v", err)
	}
}

func run(ctx context.Context) error {
	invk := &invoker.Invoker{}

	// Parse flags
	flag.Parse()

	// Load config
	conf, err := config.Load(*configPath)
	if err != nil {
		return fmt.Errorf("error loading config: %w", err)
	}

	// Build logger
	var logger *zap.Logger
	if conf.Development {
		logger, err = zap.NewDevelopment()
		if err != nil {
			return fmt.Errorf("error building logger: %w", err)
		}
	} else {
		logger, err = zap.NewProduction()
		if err != nil {
			return fmt.Errorf("error building logger: %w", err)
		}
	}

	// Handle DNS
	dnsClient, err := dns.NewClient(conf)
	if err != nil {
		return fmt.Errorf("error connecting to dns: %w", err)
	}

	// Handle certificates
	cm, err := certmanager.New(conf, logger, dnsClient)
	if err != nil {
		return fmt.Errorf("error connecting to let's encrypt: %w", err)
	}

	serv := &server.Server{
		Config:      conf,
		Logger:      logger,
		CertManager: cm,
		DnsClient:   dnsClient,
	}
	invk.Add(serv.Run)

	return invk.Run(ctx)
}
