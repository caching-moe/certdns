package server

import (
	"fmt"
	"go.uber.org/zap"
	"net/http"
)

type gencertRequest struct {
	Subdomain string `json:"subdomain"`
}

type gencertResponse struct {
	PrivateKey  []byte `json:"private_key"`
	Certificate []byte `json:"certificate"`
}

func (s *Server) GenCert(w http.ResponseWriter, r *http.Request) {
	var req gencertRequest

	logger := s.Logger.With(
		zap.String("endpoint", "gencert"),
		zap.Any("headers", r.Header),
	)

	if err := s.parseRequest(r, &req); err != nil {
		s.errorResponse(logger, w, err)
		return
	}
	logger = logger.With(zap.Any("request", req))

	fqdn := fmt.Sprintf("%s.%s", req.Subdomain, s.Config.NetworkDomain)
	key, cert, err := s.CertManager.GetCertificate(fqdn)

	if err != nil {
		s.errorResponse(logger, w, err)
	}

	resp := &gencertResponse{
		PrivateKey:  key,
		Certificate: cert,
	}

	s.sendResponse(logger, w, resp)
}
