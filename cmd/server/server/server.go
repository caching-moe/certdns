package server

import (
	"context"
	"errors"
	"gitlab.com/cachingmoe/certdns/cmd/server/certmanager"
	"gitlab.com/cachingmoe/certdns/cmd/server/config"
	"gitlab.com/cachingmoe/certdns/cmd/server/dns"

	"gitlab.com/cachingmoe/certdns/cmd/server/invoker"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"go.uber.org/zap"
)

var ErrShutdown = errors.New("graceful shutdown")

type Server struct {
	Config      *config.Config
	Logger      *zap.Logger
	CertManager *certmanager.CertManager
	DnsClient   dns.Client

	api     *http.Server
	invoker *invoker.Invoker
}

func (s *Server) Run(ctx context.Context) error {
	s.invoker = &invoker.Invoker{}
	s.invoker.Add(s.runApi)
	s.invoker.Add(s.listenForShutdown)
	return s.invoker.Run(ctx)
}

func (s *Server) runApi(ctx context.Context) error {
	mux := http.NewServeMux()
	mux.HandleFunc("/gencert", s.GenCert)
	mux.HandleFunc("/cleardns", s.ClearDns)
	mux.HandleFunc("/setdns", s.SetDns)

	s.api = &http.Server{
		Addr:         s.Config.Api.Address,
		ReadTimeout:  s.Config.Api.ReadTimeout,
		IdleTimeout:  s.Config.Api.IdleTimeout,
		WriteTimeout: s.Config.Api.WriteTimeout,
		Handler:      mux,
	}

	done := make(chan error, 1)
	go func() {
		<-ctx.Done()
		c, cancel := context.WithTimeout(context.Background(), s.Config.Api.ShutdownTimeout)
		defer cancel()
		s.Logger.Info("api server shutting down")
		done <- s.api.Shutdown(c)
	}()

	s.Logger.Info("api server starting", zap.String("address", s.Config.Api.Address))
	err := s.api.ListenAndServe()
	derr := <-done
	if err == nil || errors.Is(err, http.ErrServerClosed) {
		return derr
	}
	return err
}

func (s *Server) listenForShutdown(ctx context.Context) error {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	defer signal.Stop(c)

	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-c:
		return ErrShutdown
	}
}
