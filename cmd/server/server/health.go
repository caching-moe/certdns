package server

import (
	"net/http"

	"go.uber.org/zap"
)

type healthRequest struct {
}

type healthResponse struct {
}

func (s *Server) Health(w http.ResponseWriter, r *http.Request) {
	var req healthRequest

	logger := s.Logger.With(
		zap.String("endpoint", "health"),
		zap.Any("headers", r.Header),
	)

	if err := s.parseRequest(r, &req); err != nil {
		s.errorResponse(logger, w, err)
		return
	}
	logger = logger.With(zap.Any("request", req))

	s.sendResponse(logger, w, &healthResponse{})
}
