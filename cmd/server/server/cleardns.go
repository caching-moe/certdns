package server

import (
	"fmt"
	"go.uber.org/zap"
	"net/http"
)

type cleardnsRequest struct {
	Subdomain string `json:"subdomain"`
}

type cleardnsResposne struct {
}

func (s *Server) ClearDns(w http.ResponseWriter, r *http.Request) {
	var req cleardnsRequest

	logger := s.Logger.With(
		zap.String("endpoint", "cleardns"),
		zap.Any("headers", r.Header),
	)

	if err := s.parseRequest(r, &req); err != nil {
		s.errorResponse(logger, w, err)
		return
	}
	logger = logger.With(zap.Any("request", req))

	fqdn := fmt.Sprintf("%s.%s", req.Subdomain, s.Config.NetworkDomain)
	recordID, err := s.DnsClient.FindRecord(s.Config.NetworkDomain, fqdn, "A")

	if recordID != "" && err == nil {
		err = s.DnsClient.RemoveRecord(s.Config.NetworkDomain, recordID)
		if err != nil {
			logger.Warn("could not remove record", zap.Error(err), zap.String("fqdn", fqdn))
		}
	} else {
		logger.Debug("could not find record", zap.Error(err), zap.String("fqdn", fqdn))
	}

	resp := &cleardnsResposne{}

	s.sendResponse(logger, w, resp)
}
