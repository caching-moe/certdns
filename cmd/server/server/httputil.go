package server

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"go.uber.org/zap"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

type malformedRequest struct {
	status   int
	msg      string
	original error
}

func (mr *malformedRequest) Error() string {
	return mr.msg
}

func (mr *malformedRequest) Unwrap() error {
	return mr.original
}

func (s *Server) parseRequest(r *http.Request, dst interface{}) error {
	if r.Method != "POST" {
		return &malformedRequest{
			status: http.StatusMethodNotAllowed,
			msg:    "Method must be POST",
		}
	}

	if r.Header.Get("Content-Type") != "" {
		if !strings.HasPrefix(r.Header.Get("Content-Type"), "application/json") {
			return &malformedRequest{
				status: http.StatusUnsupportedMediaType,
				msg:    "Content-Type header is not application/json",
			}
		}
	}

	reqBody, err := ioutil.ReadAll(r.Body)

	dec := json.NewDecoder(bytes.NewBuffer(reqBody))
	dec.DisallowUnknownFields()
	if err == nil {
		err = dec.Decode(&dst)
	}

	if err != nil {
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError

		switch {
		case errors.As(err, &syntaxError):
			return &malformedRequest{
				status:   http.StatusBadRequest,
				msg:      fmt.Sprintf("Request body contains badly-formed JSON (at position %d)", syntaxError.Offset),
				original: err,
			}

		case errors.Is(err, io.ErrUnexpectedEOF):
			return &malformedRequest{
				status:   http.StatusBadRequest,
				msg:      "Request body contains badly-formed JSON",
				original: err,
			}

		case errors.As(err, &unmarshalTypeError):
			return &malformedRequest{
				status:   http.StatusBadRequest,
				msg:      fmt.Sprintf("Request body contains an invalid value for the %q field (at position %d)", unmarshalTypeError.Field, unmarshalTypeError.Offset),
				original: err,
			}

		case strings.HasPrefix(err.Error(), "json: unknown field "):
			return &malformedRequest{
				status:   http.StatusBadRequest,
				msg:      fmt.Sprintf("Request body contains unknown field %s", strings.TrimPrefix(err.Error(), "json: unknown field ")),
				original: err,
			}

		case errors.Is(err, io.EOF):
			return &malformedRequest{
				status:   http.StatusBadRequest,
				msg:      "Request body must not be empty",
				original: err,
			}

		default:
			return err
		}
	}

	err = dec.Decode(&struct{}{})
	if err != io.EOF {
		return &malformedRequest{
			status: http.StatusBadRequest,
			msg:    "Request body must only contain a single JSON object",
		}
	}

	return nil
}

func (s *Server) sendResponse(logger *zap.Logger, w http.ResponseWriter, resp interface{}) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
	logger.Debug("API response", zap.Int("status_code", 200), zap.Any("response", resp))
}

func (s *Server) errorResponse(logger *zap.Logger, w http.ResponseWriter, err error) {
	var status int
	var body string

	var mr *malformedRequest
	if errors.As(err, &mr) {
		status = mr.status
		body = mr.msg
		err = mr.original
	} else {
		status = http.StatusInternalServerError
		body = http.StatusText(http.StatusInternalServerError)
	}

	resp := struct {
		Status int    `json:"status"`
		Error  string `json:"error"`
	}{
		Status: status,
		Error:  body,
	}

	logger.Debug("API response", zap.Int("status_code", status), zap.Any("response", resp), zap.Error(err))

	// Try to make a nice JSON error message. If this fails for whatever reason we just serve plain text.
	jsonBody, err := json.Marshal(resp)
	if err == nil {
		body = string(jsonBody)
		w.Header().Set("Content-Type", "application/json")
	}

	w.WriteHeader(status)
	w.Write([]byte(body))
}
