package server

import (
	"fmt"
	"go.uber.org/zap"
	"net"
	"net/http"
)

type setdnsRequest struct {
	Subdomain string `json:"subdomain"`
	Ipv4      net.IP `json:"ipv4"`
}

type setdnsResponse struct {
}

func (s *Server) SetDns(w http.ResponseWriter, r *http.Request) {
	var req setdnsRequest

	logger := s.Logger.With(
		zap.String("endpoint", "gencert"),
		zap.Any("headers", r.Header),
	)

	if err := s.parseRequest(r, &req); err != nil {
		s.errorResponse(logger, w, err)
		return
	}
	logger = logger.With(zap.Any("request", req))

	if req.Ipv4.To4() == nil {
		s.errorResponse(logger, w, &malformedRequest{
			status: http.StatusBadRequest,
			msg:    fmt.Sprintf("%s is not an ipv4 address", req.Ipv4),
		})
	}

	fqdn := fmt.Sprintf("%s.%s", req.Subdomain, s.Config.NetworkDomain)
	err := s.DnsClient.AddRecord(s.Config.NetworkDomain, fqdn, "A", req.Ipv4.String(), 900)

	if err != nil {
		s.errorResponse(logger, w, err)
	}

	resp := &setdnsResponse{}

	s.sendResponse(logger, w, resp)
}
