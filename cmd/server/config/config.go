package config

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/pelletier/go-toml"
)

type Config struct {
	Development   bool
	DNSProvider   string
	NetworkDomain string
	Database      Database
	Api           Server
	LetsEncrypt   LetsEncrypt
	ClouDNS       ClouDNS
	Cloudflare    Cloudflare
}

type Database struct {
	File string
}

type Server struct {
	Address         string // ":8080" or "127.0.0.1:9999"
	ShutdownTimeout time.Duration
	ReadTimeout     time.Duration
	IdleTimeout     time.Duration
	WriteTimeout    time.Duration
}

type LetsEncrypt struct {
	URL   string // Blank uses the production URL
	Email string
	PEM   string
}

// https://www.cloudns.net/api-settings/ "Add new user"

type ClouDNS struct {
	AuthID       string
	AuthPassword string
}

type Cloudflare struct {
	AuthToken string
}

func Load(path string) (*Config, error) {
	// Default config
	conf := &Config{
		Development: false,
		DNSProvider: "",
		Database: Database{
			File: "database.boltdb",
		},
		LetsEncrypt: LetsEncrypt{
			URL:   "",
			Email: "",
		},
	}

	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("unable to load config file: %w", err)
	}

	dec := toml.NewDecoder(bytes.NewBuffer(data))
	dec.Strict(true)

	err = dec.Decode(conf)
	if err != nil {
		return nil, fmt.Errorf("unable to unmarshal config file: %w", err)
	}
	return conf, nil
}
