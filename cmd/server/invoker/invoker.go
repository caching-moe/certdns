package invoker

import (
	"context"
	"errors"
	"fmt"
	"runtime"
	"sync"
)

// Invoker runs goroutines in a managed fashion
type Invoker struct {
	m sync.Mutex

	ctx     context.Context
	cancel  func()
	state   invokerState
	pending []Task
	running int
	err     error
	done    chan error
}

type Task func(ctx context.Context) error

var (
	ErrRunning = errors.New("invoker is already running")
	ErrDone    = errors.New("invoker is already done")
)

type invokerState int

const (
	initializing invokerState = iota
	running
	done
)

func (i *Invoker) Add(t Task) error {
	i.m.Lock()
	defer i.m.Unlock()

	switch i.state {
	case initializing:
		i.pending = append(i.pending, t)
		return nil
	case running:
		i.running += 1
		go i.run(i.ctx, t)
		return nil
	}

	return ErrDone
}

func (i *Invoker) Run(runCtx context.Context) error {
	i.m.Lock()

	if i.state != initializing {
		i.m.Unlock()
		return ErrRunning
	}

	if len(i.pending) == 0 {
		i.state = done
		i.m.Unlock()
		return nil
	}

	ctx, cancel := context.WithCancel(runCtx)
	tasks := i.pending

	i.state = running
	i.ctx = ctx
	i.cancel = cancel
	i.running += len(tasks)
	i.done = make(chan error, 1)
	i.m.Unlock()

	// Run one task in the current goroutine as a micro-optimization
	for _, t := range tasks[1:] {
		go i.run(ctx, t)
	}
	i.run(ctx, tasks[0])

	return <-i.done
}

func (i *Invoker) run(ctx context.Context, t Task) {
	var err error // Define err early so defers can overwrite it

	defer func() {
		i.m.Lock()
		defer i.m.Unlock()

		// Set the error on Invoker and cancel other goroutines if non-nil
		if i.err == nil {
			i.err = err
		}
		if err != nil {
			i.cancel()
		}
		// Update running count and stop the Invoker if we're done
		i.running -= 1
		if i.running <= 0 {
			i.done <- i.err
			i.state = done
		}
	}()

	// Turn panics into normal errors so the server doesn't crash
	defer func() {
		if r := recover(); r != nil && err == nil {
			buf := make([]byte, 1<<10)
			runtime.Stack(buf, false)

			if e, ok := r.(error); ok {
				err = fmt.Errorf("caught panic: %w\n%s", e, buf)
			} else {
				err = fmt.Errorf("caught panic: %v\n%s", r, buf)
			}
		}
	}()

	// Run the task
	err = t(ctx)
}
