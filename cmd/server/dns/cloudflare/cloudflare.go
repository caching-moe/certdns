package cloudflare

import (
	"fmt"
	"sync"

	cloudflareAPI "github.com/cloudflare/cloudflare-go"
	"github.com/go-acme/lego/v3/challenge"
	cloudflareLego "github.com/go-acme/lego/v3/providers/dns/cloudflare"
)

func NewClient(authToken string) (*Client, error) {
	client, err := cloudflareAPI.NewWithAPIToken(authToken)
	if err != nil {
		return nil, fmt.Errorf("error creating cloudflare client: %w", err)
	}

	return &Client{
		client:    client,
		authToken: authToken,
		zoneIDs:   map[string]string{},
	}, nil
}

type Client struct {
	client       *cloudflareAPI.API
	authToken    string
	zoneIDs      map[string]string
	zoneIDsMutex sync.RWMutex
}

func (c *Client) LegoProvider() (challenge.Provider, error) {
	dnsConfig := cloudflareLego.NewDefaultConfig()
	dnsConfig.AuthToken = c.authToken
	return cloudflareLego.NewDNSProviderConfig(dnsConfig)
}

func (c *Client) FindRecord(domain string, fqdn string, recordType string) (id string, err error) {
	zoneID, err := c.zoneID(domain)
	if err != nil {
		return "", fmt.Errorf("error getting cloudflare zone id: %w", err)
	}

	records, err := c.client.DNSRecords(zoneID, cloudflareAPI.DNSRecord{
		Type: recordType,
		Name: fqdn,
	})
	if err != nil {
		return "", fmt.Errorf("error getting cloudflare records: %w", err)
	}
	if len(records) == 0 {
		return "", fmt.Errorf("got no records")
	}
	if len(records) > 1 {
		return "", fmt.Errorf("got too many records: %d", len(records))
	}

	return records[0].ID, nil
}

func (c *Client) AddRecord(domain string, fqdn string, recordType string, value string, TTL int) error {
	zoneID, err := c.zoneID(domain)
	if err != nil {
		return fmt.Errorf("error getting cloudflare zone id: %w", err)
	}

	resp, err := c.client.CreateDNSRecord(zoneID, cloudflareAPI.DNSRecord{
		Type:    recordType,
		Name:    fqdn,
		Content: value,
		TTL:     TTL,
		Proxied: false,
	})
	if err != nil {
		return err
	}
	if !resp.Success {
		return fmt.Errorf("failed to create record: %+v %+v", resp.Errors, resp.Messages)
	}

	return nil
}

func (c *Client) RemoveRecord(domain string, recordID string) error {
	zoneID, err := c.zoneID(domain)
	if err != nil {
		return fmt.Errorf("error getting cloudflare zone id: %w", err)
	}

	return c.client.DeleteDNSRecord(zoneID, recordID)

}

func (c *Client) zoneID(domain string) (string, error) {
	c.zoneIDsMutex.RLock()
	id, ok := c.zoneIDs[domain]
	c.zoneIDsMutex.RUnlock()
	if ok {
		return id, nil
	}

	id, err := c.client.ZoneIDByName(domain)
	if err != nil {
		return "", err
	}

	c.zoneIDsMutex.Lock()
	c.zoneIDs[domain] = id
	c.zoneIDsMutex.Unlock()

	return id, nil
}
