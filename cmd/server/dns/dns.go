package dns

import (
	"fmt"

	"github.com/go-acme/lego/v3/challenge"

	"gitlab.com/cachingmoe/certdns/cmd/server/config"
	"gitlab.com/cachingmoe/certdns/cmd/server/dns/cloudflare"
	"gitlab.com/cachingmoe/certdns/cmd/server/dns/cloudns"
)

type Client interface {
	LegoProvider() (challenge.Provider, error)
	FindRecord(domain string, fqdn string, recordType string) (id string, err error)
	AddRecord(domain string, fqdn string, recordType string, value string, TTL int) error
	RemoveRecord(domain string, recordID string) error
}

func NewClient(conf *config.Config) (Client, error) {
	switch conf.DNSProvider {
	case "cloudns":
		return cloudns.NewClient(conf.ClouDNS.AuthID, conf.ClouDNS.AuthPassword)
	case "cloudflare":
		return cloudflare.NewClient(conf.Cloudflare.AuthToken)
	default:
		return nil, fmt.Errorf("invalid DNSProvider given: %v", conf.DNSProvider)
	}
}
