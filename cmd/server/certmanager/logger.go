package certmanager

import (
	legolog "github.com/go-acme/lego/v3/log"
	"go.uber.org/zap"
)

type logger struct {
	logger *zap.SugaredLogger
}

// Assert we meet the interface
var _ legolog.StdLogger = (*logger)(nil)

func (l *logger) Fatal(args ...interface{}) {
	l.logger.Fatal(args...)
}

func (l *logger) Fatalln(args ...interface{}) {
	l.logger.Fatal(args...)
}

func (l *logger) Fatalf(format string, args ...interface{}) {
	l.logger.Fatalf(format, args...)
}

func (l *logger) Print(args ...interface{}) {
	l.logger.Debug(args...)
}

func (l *logger) Println(args ...interface{}) {
	l.logger.Debug(args...)
}

func (l *logger) Printf(format string, args ...interface{}) {
	l.logger.Debugf(format, args...)
}
