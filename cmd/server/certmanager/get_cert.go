package certmanager

import (
	"github.com/go-acme/lego/v3/certificate"
)

func (cm *CertManager) GetCertificate(domain string) (key []byte, cert []byte, err error) {
	r, err := cm.client.Certificate.Obtain(certificate.ObtainRequest{
		Domains:    []string{domain},
		Bundle:     true,
		MustStaple: false,
	})
	if err != nil {
		return nil, nil, err
	}
	return r.PrivateKey, r.Certificate, nil
}
