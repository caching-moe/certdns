package certmanager

import (
	"crypto"
	"crypto/x509"
	"encoding/pem"
	"fmt"

	"github.com/go-acme/lego/v3/lego"
	legolog "github.com/go-acme/lego/v3/log"
	"github.com/go-acme/lego/v3/registration"
	"go.uber.org/zap"

	"gitlab.com/cachingmoe/certdns/cmd/server/config"
	"gitlab.com/cachingmoe/certdns/cmd/server/dns"
)

type CertManager struct {
	config *config.Config
	client *lego.Client
}

func New(conf *config.Config, l *zap.Logger, dnsClient dns.Client) (*CertManager, error) {
	legolog.Logger = &logger{l.Sugar()}

	user := &acmeUser{
		id:    append([]byte(conf.LetsEncrypt.Email), []byte(conf.LetsEncrypt.URL)...),
		Email: conf.LetsEncrypt.Email,
	}

	keyBlock, _ := pem.Decode([]byte(conf.LetsEncrypt.PEM))
	if keyBlock == nil {
		return nil, fmt.Errorf("error decoding PEM key from config")
	}

	switch keyBlock.Type {
	case "RSA PRIVATE KEY":
		user.key, _ = x509.ParsePKCS1PrivateKey(keyBlock.Bytes)
	case "EC PRIVATE KEY":
		user.key, _ = x509.ParseECPrivateKey(keyBlock.Bytes)
	}

	// Create LEGO client
	clientConfig := lego.NewConfig(user)
	if conf.LetsEncrypt.URL != "" {
		clientConfig.CADirURL = conf.LetsEncrypt.URL
	}
	client, err := lego.NewClient(clientConfig)
	if err != nil {
		return nil, fmt.Errorf("error creating lego client: %w", err)
	}

	// Initialize DNS challenge resolver
	provider, err := dnsClient.LegoProvider()
	if err != nil {
		return nil, fmt.Errorf("error creating lego dns provider: %w", err)
	}
	err = client.Challenge.SetDNS01Provider(provider)
	if err != nil {
		return nil, fmt.Errorf("error setting challenge to lego dns provider: %w", err)
	}

	// Register if we need to
	if user.Registration == nil {
		reg, err := client.Registration.Register(registration.RegisterOptions{TermsOfServiceAgreed: true})
		if err != nil {
			return nil, fmt.Errorf("error registering user: %w", err)
		}
		user.Registration = reg
	}

	return &CertManager{
		config: conf,
		client: client,
	}, nil
}

// Implements acme.User
type acmeUser struct {
	id           []byte
	Email        string
	Registration *registration.Resource
	key          crypto.PrivateKey
}

func (u *acmeUser) GetEmail() string {
	return u.Email
}
func (u acmeUser) GetRegistration() *registration.Resource {
	return u.Registration
}
func (u *acmeUser) GetPrivateKey() crypto.PrivateKey {
	return u.key
}
