FROM golang:alpine AS build

WORKDIR /tmp/certdns

COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download

COPY cmd/server cmd/server
RUN CGO_ENABLED=0 GOOS=linux go build -o server ./cmd/server

FROM alpine

WORKDIR /opt/certdns

COPY --from=build /tmp/certdns/server /opt/certdns/server

ENTRYPOINT [ "/opt/certdns/server" ]
