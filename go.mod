module gitlab.com/cachingmoe/certdns

go 1.16

require (
	github.com/cloudflare/cloudflare-go v0.13.8
	github.com/go-acme/lego/v3 v3.9.0
	github.com/miekg/dns v1.1.40 // indirect
	github.com/pelletier/go-toml v1.8.1
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
